<?php
require_once 'app/MovieController.php';

// instancio la clase del controlador
$controller = new MovieController();

// simulamos un router
if (isset($_GET['genre'])) {
    $controller->showMoviesByGenre();
}
else {
    $controller->showMovies();
}