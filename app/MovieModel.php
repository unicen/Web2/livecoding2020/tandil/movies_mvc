<?php

class MovieModel {

    private $db;

    public function __construct() {
        $this->db = new PDO('mysql:host=localhost;'.'dbname=db_movies;charset=utf8', 'root', '');
    }

    /**
     *  Obtiene la lista de peliculas de la DB según género
     */
    function getMoviesByGenero($genre) {
        $query = $this->db->prepare('SELECT * FROM movies WHERE genre = ?');
        $query->execute([$genre]); // array($genre)
        $movies = $query->fetchAll(PDO::FETCH_OBJ);
        return $movies;
    }

    /**
     * Obtiene todas las peliculas
     */
    function getMovies() {
        $query = $this->db->prepare('SELECT * FROM movies');
        $query->execute();
        $movies = $query->fetchAll(PDO::FETCH_OBJ);
        return $movies;
    }
}